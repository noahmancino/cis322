Author: Noah Mancino
Email: nmmancin2@uoregon.edu

This project consists of an ini file, a python program that 
prints the value of the ini file's message key, and a Makefile that
executes the python program.  
